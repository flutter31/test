import 'package:flutter/material.dart';

import 'package:cources/interfaces/keyboard-setting.dart';
import 'package:cources/interfaces/button-setting.dart';
import 'package:cources/enums/index.dart';

class MainModel {
  /* PATHES */
  static const String arrowLeftIconPath = 'assets/images/arrow-left.png';
  /* DIMENSIONS */
  static const double buttonHeight = 54.2;
  static const double buttonWidth = 68;
  /* COLORS */
  static const Color lightBlueColor = Color.fromRGBO(0, 91, 228, 0.16);
  static const Color mediumGrayColor = Color.fromRGBO(138, 138, 138, 0.16);
  static const Color lightGrayColor = Color.fromRGBO(138, 138, 138, 0.1);
  static const Color grayColor = Color.fromRGBO(167, 148, 148, 0.6);
  static const Color darkGrayColor = Color.fromRGBO(160, 149, 149, 0.75);
  static const Color darkestGrayColor = Color(0xFF988D8D);
  static const Color fontColor = Color.fromRGBO(0, 0, 0, 0.24);
  static const Color inverseFontColor = Color(0xFFF4EBE5);
  static const Color displayColor = Color(0xFFA09595);
  /* DATA */
  static const Map<OperationType, String> operations = {
    OperationType.divide: '÷',
    OperationType.minus: '-',
    OperationType.multiply: 'x',
    OperationType.plus: '+'
  };
  /* GETTERS */
  static KeyboardSetting createKeyboardSetting(Map<ButtonType, void Function(String)> onClickFactory) {
    return KeyboardSetting(
        [
          /* 1st row */
          ButtonSetting(value: 'C', type: ButtonType.clear),
          ButtonSetting(
              value: MainModel.operations[OperationType.divide],
              type: ButtonType.operation
          ),
          ButtonSetting(
              value: MainModel.operations[OperationType.multiply],
              type: ButtonType.operation
          ),
          ButtonSetting(
            customContent: Image(image: AssetImage(MainModel.arrowLeftIconPath)),
            backgroundColor: MainModel.grayColor,
            type: ButtonType.backspace
          ),

          /* 2nd row */
          ButtonSetting(backgroundColor: MainModel.mediumGrayColor, value: '7'),
          ButtonSetting(backgroundColor: MainModel.mediumGrayColor, value: '8'),
          ButtonSetting(backgroundColor: MainModel.mediumGrayColor, value: '9'),
          ButtonSetting(
              backgroundColor: MainModel.darkGrayColor,
              fontColor: MainModel.inverseFontColor,
              value: MainModel.operations[OperationType.minus],
              type: ButtonType.operation
          ),

          /* 3th row */
          ButtonSetting(value: '4'),
          ButtonSetting(value: '5'),
          ButtonSetting(value: '6'),
          ButtonSetting(
              backgroundColor: MainModel.grayColor,
              fontColor: MainModel.inverseFontColor,
              value: MainModel.operations[OperationType.plus],
              type: ButtonType.operation
          ),

          /* 4th row */
          ButtonSetting(backgroundColor: MainModel.mediumGrayColor, value: '1'),
          ButtonSetting(backgroundColor: MainModel.mediumGrayColor, value: '2'),
          ButtonSetting(backgroundColor: MainModel.mediumGrayColor, value: '3'),
          ButtonSetting(
              backgroundColor: MainModel.darkGrayColor,
              fontColor: MainModel.inverseFontColor,
              value: '()',
              type: ButtonType.brackets
          ),

          /* 5th row */
          ButtonSetting(value: '0'),
          ButtonSetting(value: '.', type: ButtonType.point),
          ButtonSetting(value: '+/-', type: ButtonType.sign),
          ButtonSetting(
              backgroundColor: MainModel.darkestGrayColor,
              fontColor: MainModel.inverseFontColor,
              value: '=',
              type: ButtonType.calculate
          ),
        ],
        4,
        MainModel.buttonWidth / MainModel.buttonHeight,
        onClickFactory
    );
  }
}