enum ButtonType {
  clear,
  backspace,
  calculate,
  point,
  brackets,
  sign,
  digit,
  operation,
}

enum OperationType {
  divide,
  multiply,
  plus,
  minus
}