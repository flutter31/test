import 'package:flutter/material.dart';

import 'package:cources/globals/main.dart';
import 'package:cources/enums/index.dart';

class ButtonSetting<T extends String, int> {
  final Color backgroundColor;
  final Color fontColor;
  final double fontSize;
  final FontWeight fontWeight;
  final T value;
  final Widget customContent;
  final ButtonType type;

  final void Function(dynamic) onClick;

  Center content;

  TextStyle _textStyle;

  ButtonSetting({
    this.backgroundColor = MainModel.lightGrayColor,
    this.fontColor = MainModel.fontColor,
    this.fontSize = 18,
    this.fontWeight = FontWeight.bold,
    this.type = ButtonType.digit,
    this.onClick,
    this.value,
    this.customContent
  }) : super() {

    this._textStyle = TextStyle(
      color: this.fontColor,
      fontSize: this.fontSize,
      fontWeight: this.fontWeight
    );
    this.content = Center(
      child: this.customContent != null ? this.customContent : Text(
          this.value.toString(),
          style: this._textStyle
      ),
    );
  }
}