import 'package:cources/enums/index.dart';

class NumberSetting {
  double _value;
  bool isInt = true;

  NumberSetting(String stringValue){
    this._value = double.parse(stringValue);
  }

  String get value => this.isInt
      ? this._value.toStringAsFixed(0)
      : this._value.toString();

  set value(String stringValue) => this._value = double.parse(stringValue);

  NumberSetting operate(NumberSetting number, OperationType operation) {
    double result;
    switch (operation) {
      case OperationType.minus: {
        result = this._value - double.parse(number.value);
      }
      break;

      case OperationType.plus: {
        result = this._value + double.parse(number.value);
      }
      break;

      case OperationType.multiply: {
        result = this._value * double.parse(number.value);
      }
      break;

      case OperationType.divide: {
        result = this._value / double.parse(number.value);
      }
      break;
    }

    return NumberSetting(result.toString());
  }
}

