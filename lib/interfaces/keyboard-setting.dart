import 'package:cources/interfaces/button-setting.dart';
import 'package:cources/enums/index.dart';

class KeyboardSetting {
  final List<ButtonSetting> buttons;
  final Map<ButtonType, void Function(String)> onClickFactory;
  final int rowWeight;
  final double ratio;

  KeyboardSetting(this.buttons, this.rowWeight, this.ratio, this.onClickFactory){}
}