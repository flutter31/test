import 'package:cources/globals/main.dart';
import 'package:flutter/material.dart';

class Display extends StatefulWidget {
  String value;

  Display(this.value) : super();

  @override
  _DisplayState createState() => _DisplayState();
  
}

class _DisplayState extends State<Display> {
  @override
  Widget build(BuildContext context) {
    return  Container(
      color: MainModel.displayColor,
      child: Center(
        child: Text(
          widget.value,
          style: TextStyle(fontSize: 72, color: MainModel.inverseFontColor),
        ),
      ),
    );
  }
}