import 'package:flutter/material.dart';

import 'package:cources/interfaces/button-setting.dart';

class Button extends StatelessWidget {
  final ButtonSetting setting;

  final void Function(ButtonSetting) onClick;

  Button(this.setting, this.onClick) : super() {}

  @override
  Widget build(BuildContext context) {
    return  GestureDetector(
        onTap: (){
          this._onClick();
        },
        child: new Container(
          padding: new EdgeInsets.all(10),
          color: this.setting.backgroundColor,
          child: this.setting.content,
        )
    );
  }

  void _onClick() { this.onClick(this.setting); }
}
