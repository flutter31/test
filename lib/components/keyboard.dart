import 'package:flutter/material.dart';

import 'package:cources/interfaces/keyboard-setting.dart';
import 'package:cources/interfaces/button-setting.dart';
import 'package:cources/enums/index.dart';
import 'package:cources/components/button.dart';
import 'package:cources/components/digit_button.dart';
import 'package:cources/components/operation_button.dart';

class Keyboard extends StatelessWidget {
  final KeyboardSetting settings;

  Keyboard(this.settings): super();

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      shrinkWrap: true,
      crossAxisCount: this.settings.rowWeight,
      childAspectRatio: this.settings.ratio,
      crossAxisSpacing: 0,
      mainAxisSpacing: 0,
      padding: const EdgeInsets.all(0),
      children: List.generate(
        this.settings.buttons.length,
        (index) =>  this._buildButton(index)
      )
    );
  }

  void onButtonClick(ButtonSetting button) {
    this.settings.onClickFactory[button.type](button.value);
  }

  Button _buildButton(int index) {
    ButtonSetting setting = this.settings.buttons[index];
    switch (setting.type) {
      case ButtonType.digit: {
        return DigitButton(setting, this.onButtonClick);
      }
      break;
      case ButtonType.operation: {
        return OperationButton(setting, this.onButtonClick);
      }
      break;
      default: return Button(setting, this.onButtonClick);
    }
  }
}