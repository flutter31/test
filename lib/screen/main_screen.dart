import 'package:flutter/material.dart';

import 'package:cources/globals/main.dart';

import 'package:cources/components/display.dart';
import 'package:cources/components/keyboard.dart';
import 'package:cources/enums/index.dart';
import 'package:cources/interfaces/number-settings.dart';

class MainScreen extends StatefulWidget {

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  Map<ButtonType, void Function(String)> onClickFactory;
  List<NumberSetting> _numbers = [];
  List<OperationType> _operations = [];

  String get displayValue {
    String value = '';
    for(int i = 0; i < this._numbersCount; i++) {
      value += this._numbers[i].value;
      if (i < this._operationsCount) {
        value += MainModel.operations[this._operations[i]];
      }
    }
    return value;
  }

  int get _numbersCount => _numbers.length;

  int get _operationsCount => _operations.length;

  String get _currentNumberValue => this._numbers[this._numbersCount - 1].value;

  NumberSetting get _result {
    NumberSetting result = this._numbers[0].operate(
      this._numbers[1],
      this._operations[0]
    );
    for(int i = 1; i < this._numbersCount - 1; i++) {
      result = result.operate(this._numbers[i + 1], this._operations[i]);
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    onClickFactory = {
      ButtonType.clear: clearDisplay,
      ButtonType.digit: onClickDigit,
      ButtonType.backspace: backspace,
      ButtonType.operation: onClickOperation,
      ButtonType.calculate: calculate
    };
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(child: Display(this.displayValue)),
          Container(child: Keyboard(MainModel.createKeyboardSetting(onClickFactory))
          ),
        ],
      ),
    );
  }


  void onClickDigit(String digit) {
    setState(() {
      this._numbersCount == this._operationsCount
        ? this._numbers.add(NumberSetting(digit))
        : this._numbers[this._numbersCount - 1].value = (this._currentNumberValue + digit);
    });
  }

  void onClickOperation(String operationSymbol) {
    setState(() {
      if (this._numbersCount == this._operationsCount) {
        return;
      };
      for (var entry in MainModel.operations.entries) {
        if (entry.value == operationSymbol) {
          this._operations.add(entry.key);
        }
      }
    });
  }
  
  void backspace(String _) {
    setState(() {
      if (this._numbersCount == 0) {
        return;
      }
      this._numbersCount == this._operationsCount
        ? this._operations.removeLast()
        : this._currentNumberValue.length > 1
          ? this._numbers[this._numbersCount - 1].value = this._currentNumberValue.substring(
              0,
              this._currentNumberValue.length - 1
          )
          : this._numbers.removeLast();
    });
  }

  void clearDisplay(String _) {
    setState(() {
      this._numbers = [];
      this._operations = [];
    });
  }

  void calculate(String _) {
    setState(() {
      if (this._numbersCount > 1) {
        NumberSetting result = this._result;
        this.clearDisplay(_);
        this._numbers.add(result);
      }
    });
  }
}
